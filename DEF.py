def saxpy(a, x, y):
    z = [x * a + y for x, y in zip(x, y)]
    return z
