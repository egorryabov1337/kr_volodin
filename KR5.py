from def5 import cramer, gauss, random_matrix
import random as rd
import time
from sys import getsizeof


type_matrix = input(f"Выберите тип матрицы: 1 - целочисленная; 2 - вещественная >>> ")
method = input("Выберите метод подсчет: 1 - Крамер; 2 - Гаусс-Джорданн >>> ")

file = open("KR5.txt", 'w')

if method == '1':
    n = 1
    n_stop = 9
    step = 1
    while n <= n_stop:

        time_data = []  # список для хранения значения занимаемого времени
        averages = []  # список для хранения среднего значения занимаемого времени

        matrix = random_matrix(method, type_matrix, n)  # создаем матрицу с помощью random_matrix()
        answers = [rd.randint(0, 9) for i in range(n)]
        for i in range(3):

            time_start = time.time()
            result = cramer(matrix, answers, n)
            time_stop = time.time()

            time_result = time_stop - time_start  # конечный результат времени
            time_data.append(time_result)

            print(f'для матрицы с размерностью {n} время заняло {time_result}...')
        averages.append(sum(time_data) / 3)

        file.write(f'{str(averages[-1]).replace(".", ",")}\n')
        n += step


elif method == '2':
    n = 10
    n_stop = 110
    step = 10
    while n <= n_stop:

        time_data = []  # список для хранения значения занимаемого времени
        averages = []  # список для хранения среднего значения занимаемого времени
        matrix = random_matrix(method, type_matrix, n)  # создаем матрицу с помощью random_matrix()
        for i in range(3):

            time_start = time.time()
            result = gauss(matrix, n)  # вывод результата с помощью gauss()
            time_stop = time.time()

            time_result = time_stop - time_start  # конечный результат времени
            time_data.append(time_result)

            print(f'для матрицы с размерностью {n} время заняло {time_result}...')
        averages.append(sum(time_data) / 3)

        print(f"Memory usage in {n} matrix = {getsizeof(result)} byte...")

        file.write(f'{str(averages[-1]).replace(".", ",")}\n')
        n += step


file.close()

