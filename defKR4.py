from random import randint, uniform


def c_matrix(type_, size):
    if type_ == '1':
        # создание матрицы, короторая зависит от размерности size c входящими в нее элементами от 0 до 9
        a = [[randint(0, 9) for element in range(size)] for j in range(size)]
    elif type_ == '2':
        a = [[uniform(0, 9) for element in range(size)] for j in range(size)]
    return a


def m_matrix(matrix1, matrix2):

    z = [[0 for e in range(len(matrix1))] for r in range(len(matrix2[0]))]  # массив из строк 1 матрицы и столбцов 2

    for i in range(len(matrix1)):
        for j in range(len(matrix2[0])):
            for k in range(len(matrix2)):
                z[i][j] += matrix1[i][k] * matrix2[k][j]
    return z
