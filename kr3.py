from random import randint, uniform
from DEF import saxpy
from time import time

type_vector = input('Выберите тип вектора 1 - целочисленный, 2 - вещественный >>> ')
scalar_a = int(input('Введите значение для скаляра a >>> '))

n = 100000  # 1e5; n - колличество элементов вектора
n_step = 100000  # 1e5
n_stop = 1000000  # 1e6

x = y = 0
file = open('time.txt', 'w')

while n <= n_stop:

    try:
        if type_vector == '1':
            x = [randint(0, 10) for i in range(n)]
            y = [randint(0, 10) for i in range(n)]
        elif type_vector == '2':
            x = [uniform(0, 10) for flt in range(n)]
            y = [uniform(0, 10) for flt in range(n)]
        else:
            raise ValueError  # поднимает исключение

    except ValueError:
        print('Выберите 1 или 2...')
        exit()

    start_time = time()

    print(f'z = {saxpy(scalar_a, x, y)}')  # выводит на экран конечный результат

    stop_time = time()
    time_result = str(stop_time - start_time).replace('.', ',')
    file.write(f'{time_result}\n')
    n += n_step

file.close()